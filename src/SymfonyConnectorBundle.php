<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyConnectorBundle extends Bundle
{
}
