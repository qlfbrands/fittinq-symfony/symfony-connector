<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\Logging;

interface LoggingAwareInterface
{
    /* This interface works as an opt-in switch to turn on logging for commands and controllers. Simply implement the
     * interface and follow the rest of the configuration in the README.md.
     */
}
