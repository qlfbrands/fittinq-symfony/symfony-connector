<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\Logging;

use Iterator;
use Throwable;

class MessageFormatter
{
    /**
     * @var string[]
     */
    private array $sensitiveHeaders;

    /**
     * @var string[]
     */
    private array $sensitiveUrls;

    public function __construct(array $sensitiveUrls, array $sensitiveHeaders)
    {
        $this->sensitiveUrls = $sensitiveUrls;
        $this->sensitiveHeaders = $sensitiveHeaders;
    }

    public function createRequestMessage(string $method, string $url, Iterator $headers, string $content): string
    {
        $message = "{$method} {$url}\n\n";
        $message .= $this->getFormattedHeaders($url, $headers);
        $message .= $this->urlIsSensitive($url) ?  '*******' : $content;

        return rtrim($message);
    }

    public function createResponseMessage(string $method, string $url, int $statusCode, Iterator $headers, string $content): string
    {
        $message = "{$method} {$url}\n\n";
        $message .= "HTTP {$statusCode}\n\n";
        $message .= $this->getFormattedHeaders($url, $headers);
        $message .= $this->urlIsSensitive($url) ?  '*******' : $content;

        return $message;
    }

    private function getFormattedHeaders(string $url, Iterator $headerBag): string
    {
        $headers = '';

        foreach ($headerBag as $key => $header) {
            // Symfony returns our headers in an array, which is a bit weird. we unpack it here.
            $header = is_array($header) ? $header[0] : $header;

            if ($this->urlIsSensitive($url) || $this->headerIsSensitive($key)) {
                $headers .= "{$key}: *******\n";
            } else {
                $headers .= "{$key}: {$header}\n";
            }
        }

        return ltrim($headers . "\n");
    }

    public function createResponseServiceUnavailableMessage(string $method, string $url, Throwable $e): string
    {
        $message = "{$method} {$url}\n\n";
        $message .= 'Service is unavailable: ' . $e->getMessage();
        return $message;
    }

    private function urlIsSensitive(string $url): bool
    {
        return in_array($url, $this->sensitiveUrls);
    }

    private function headerIsSensitive(string $key): bool
    {
        $sensitiveHeadersLowercase = array_map(function ($sensitiveHeader){
            return strtolower($sensitiveHeader);
        }, $this->sensitiveHeaders);

        return in_array(strtolower($key), $sensitiveHeadersLowercase);
    }
}
