<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\Logging;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ControllerLogger extends HttpLogger
{
    public function logRequest(Request $request): void
    {
        $this->logger->info($this->messageFormatter->createRequestMessage(
            $request->getMethod(),
            $request->getUri(),
            $request->headers->getIterator(),
            $request->getContent()
        ));
    }

    public function logResponse(Request $request, Response $response): void
    {
        $message = $this->messageFormatter->createResponseMessage(
            $request->getMethod(),
            $request->getUri(),
            $response->getStatusCode(),
            $response->headers->getIterator(),
            $response->getContent()
        );

        $this->logResponseWithSeverity($message, $response->getStatusCode());
    }
}
