<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\ElasticSearch;

use Fittinq\Logger\Context\ContextResolver;

class ServiceContext extends ContextResolver
{
    public static string $PID;

    public function __construct(string $service)
    {
        parent::__construct([
            'pid' => $this->getUniquePid(),
            'service' => $service
        ]);
    }

    private function getUniquePid(): string
    {
        if (!isset(self::$PID)) {
            self::$PID = uniqid();
        }

        return self::$PID;
    }
}
