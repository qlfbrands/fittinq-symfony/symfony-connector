# Symfony connector
Use symfony connector to handle http requests.

## Install via composer
```bash
composer require fittinq\symfony-connector
```
## Configure bundle
```php
# config/bundles.php
<?php

return [
    // ...
    Fittinq\Symfony\Connector\SymfonyConnectorBundle::class => ['all' => true]
];

```

## Configure .env
Configure the following .env.local settings
```ini
APP_ENV=prod

CONNECTOR_SENSITIVE_HEADERS='["Authorization"]'
CONNECTOR_INDEX_NAME=hello_
CONNECTOR_INDEX_DATE_FORMAT=Ymd
CONNECTOR_SERVICE_NAME=connector-hello-world

CONNECTOR_ELASTIC_LOGGER_HOST_URL=https://username:password@elasticsearch.io:9243
```
## Configure custom log service
This service introduces a new logger alias 'fittinq.logger'. This references '@logger' by default, but can be overridden
in the service.yml:
```yaml
# config/services.yaml
services:
    fittinq.logger: '@Fittinq\Logger\Logger\ElasticSearchLogger'
```