<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Command;

use Symfony\Component\Console\Command\Command;

class NotLoggingAwareCommand extends Command
{
}