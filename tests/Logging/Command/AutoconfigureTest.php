<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Command;

use Fittinq\Symfony\Connector\EventSubscriber\CommandEventSubscriber;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AutoconfigureTest extends TestCase
{
    public function test_autoConfigureBeforeAndAfterCommandEvents_expectInstanceOfEventSubscriber()
    {
        $configuration = new Configuration();

        $this->assertInstanceOf(
            EventSubscriberInterface::class,
            $configuration->create([], [], new LoggingAwareCommand())
        );
    }

    public function test_autoConfigureBeforeAndAfterCommandEvents_expectBeforeAndAfterControllerHooks()
    {
        $events = CommandEventSubscriber::getSubscribedEvents();
        $this->assertArrayHasKey(ConsoleEvents::COMMAND , $events);
        $this->assertArrayHasKey(ConsoleEvents::ERROR , $events);
        $this->assertArrayHasKey(ConsoleEvents::TERMINATE , $events);
    }
}
