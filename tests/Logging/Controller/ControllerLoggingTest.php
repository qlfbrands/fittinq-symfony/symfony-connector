<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

use Fittinq\Symfony\Connector\EventSubscriber\ControllerEventSubscriber;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;
use Throwable;

class ControllerLoggingTest extends TestCase
{
    private Configuration $configuration;
    private LoggerMock $loggerMock;
    private ControllerEventSubscriber $eventSubscriber;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->loggerMock = $this->configuration->getLogger();
    }

    public function test_doNotLogRequestOrResponseForNotLoggingAwareController()
    {
        $this->eventSubscriber = $this->configuration->configure(
            new Request(),
            new Response(),
            new NotLoggingAwareController(),
        );

        $this->onKernelController();
        $this->onKernelResponse();

        $this->loggerMock->expectNotToLogAnything();
    }

    public function test_logRequestForLoggingAwareController()
    {
        $this->eventSubscriber = $this->configuration->configure(
            Request::create(
                'https://fittinq.com/theControllerIsALoggingAwareController',
                Request::METHOD_POST,
                [],
                [],
                [],
                [
                    "CONTENT_TYPE" => 'application/json',
                    "HTTP_X_AUTH_TOKEN" => 'token_0123456789',
                    "HTTP_AUTHORIZATION" => '[type] [credentials]'
                ],
                '{"key": "value"}'
            ),
            new Response(),
            new LoggingAwareController(),
        );
        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            POST https://fittinq.com/theControllerIsALoggingAwareController
            
            host: fittinq.com
            user-agent: Symfony
            accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
            accept-language: en-us,en;q=0.5
            accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
            content-type: application/json
            x-auth-token: token_0123456789
            authorization: [type] [credentials]
            
            {"key": "value"}
            EOT
        );
    }

    public function test_omitBodyForGETRequestWithoutBody()
    {
        $this->eventSubscriber = $this->configuration->configure(
            Request::create(
                'https://fittinq.com/makeGETRequestWithoutBody',
                Request::METHOD_GET,
                [],
                [],
                [],
                ["CONTENT_TYPE" => 'text/xml']
            ),
            new Response(),
            new LoggingAwareController(),
            [],
            []
        );
        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            GET https://fittinq.com/makeGETRequestWithoutBody
            
            host: fittinq.com
            user-agent: Symfony
            accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
            accept-language: en-us,en;q=0.5
            accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
            content-type: text/xml
            EOT
        );
    }

    public function test_makeGETRequestWithoutHeaders_omitHeaders()
    {
        $request = Request::create(
            'https://fittinq.com/makeGETRequestWithoutHeaders',
            Request::METHOD_GET
        );
        $request->headers = new HeaderBag();

        $this->eventSubscriber = $this->configuration->configure(
            $request,
            new Response(),
            new LoggingAwareController(),
            []
        );
        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            GET https://fittinq.com/makeGETRequestWithoutHeaders
            EOT
        );
    }

    public function test_makeGETRequestWithQueryParameters()
    {
        $request = Request::create(
            'https://fittinq.com/makeGETRequestWithQueryParameters?bicycle=notrain',
            Request::METHOD_GET,
            ['car' => 'noTrainEither']
        );
        $request->headers = new HeaderBag();

        $this->eventSubscriber = $this->configuration->configure(
            $request,
            new Response(),
            new LoggingAwareController(),
            []
        );
        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            GET https://fittinq.com/makeGETRequestWithQueryParameters?bicycle=notrain&car=noTrainEither
            EOT
        );
    }

    public function test_omitHeadersForRequestWithoutHeaders()
    {
        $request = Request::create(
            'https://fittinq.com/makePostRequestWithoutHeaders',
            Request::METHOD_POST,
            [],
            [],
            [],
            [],
            '{"key": "value"}'
        );
        $request->headers = new HeaderBag();

        $this->eventSubscriber = $this->configuration->configure(
            $request,
            new Response(),
            new LoggingAwareController(),
            []
        );
        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            POST https://fittinq.com/makePostRequestWithoutHeaders
            
            {"key": "value"}
            EOT
        );
    }

    public function test_censorSensitiveHeadersForRequest()
    {
        $this->eventSubscriber = $this->configuration->configure(
            Request::create(
                'https://fittinq.com/censorSensitiveHeaders',
                Request::METHOD_POST,
                [],
                [],
                [],
                [
                    "CONTENT_TYPE" => 'application/json',
                    "HTTP_X_AUTH_TOKEN" => 'token_0123456789',
                    "HTTP_AUTHORIZATION" => '[type] [credentials]'
                ],
                '{"key": "value"}'
            ),
            new Response(),
            new LoggingAwareController(),
            [],
            ['x-auth-token', 'authorization']
        );

        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            POST https://fittinq.com/censorSensitiveHeaders
            
            host: fittinq.com
            user-agent: Symfony
            accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
            accept-language: en-us,en;q=0.5
            accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
            content-type: application/json
            x-auth-token: *******
            authorization: *******
            
            {"key": "value"}
            EOT
        );
    }

    public function test_whenSensitiveHeaderHasCapsLowercaseInComparison()
    {
        $this->eventSubscriber = $this->configuration->configure(
            Request::create(
                'https://fittinq.com/censorSensitiveHeaders',
                Request::METHOD_POST,
                [],
                [],
                [],
                [
                    "CONTENT_TYPE" => 'application/json',
                    "HTTP_X_AUTH_TOKEN" => 'token_0123456789',
                    "HTTP_AUTHORIZATION" => '[type] [credentials]'
                ],
                '{"key": "value"}'
            ),
            new Response(),
            new LoggingAwareController(),
            [],
            ['x-auth-token', 'Authorization']
        );

        $this->onKernelController();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            POST https://fittinq.com/censorSensitiveHeaders
            
            host: fittinq.com
            user-agent: Symfony
            accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
            accept-language: en-us,en;q=0.5
            accept-charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7
            content-type: application/json
            x-auth-token: *******
            authorization: *******
            
            {"key": "value"}
            EOT
        );
    }

    public function test_censorSensitiveHeadersForResponse()
    {
        $request = Request::create(
            'https://fittinq.com/censorSensitiveHeaders',
            Request::METHOD_POST
        );

        $response = new Response(
            '{"success": "true"}',
            Response::HTTP_OK,
            [
                "content-type" => 'application/json',
                "x-auth-token" => 'token_0123456789',
                "authorization" => '[type] [credentials]'
            ]
        );
        $date = $response->getDate()->format('D, d M Y H:i:s'). ' GMT';

        $this->eventSubscriber = $this->configuration->configure(
            $request,
            $response,
            new LoggingAwareController(),
            [],
            ['x-auth-token', 'authorization']
        );
        $this->onKernelController();
        $this->onKernelResponse();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            POST https://fittinq.com/censorSensitiveHeaders

            HTTP 200

            content-type: application/json
            x-auth-token: *******
            authorization: *******
            cache-control: no-cache, private
            date: $date

            {"success": "true"}
            EOT
        );
    }

    public function test_logResponseForLoggingAwareController()
    {
        $request = Request::create(
            'https://fittinq.com/theControllerIsALoggingAwareController',
            Request::METHOD_POST
        );

        $response = new Response(
            '{"success": "true"}',
            Response::HTTP_OK,
            [
                "content-type" => 'application/json',
                "x-auth-token" => 'token_0123456789',
                "authorization" => '[type] [credentials]'
            ]
        );
        $date = $response->getDate()->format('D, d M Y H:i:s'). ' GMT';

        $this->eventSubscriber = $this->configuration->configure($request, $response, new LoggingAwareController());
        $this->onKernelController();
        $this->onKernelResponse();

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            POST https://fittinq.com/theControllerIsALoggingAwareController

            HTTP 200

            content-type: application/json
            x-auth-token: token_0123456789
            authorization: [type] [credentials]
            cache-control: no-cache, private
            date: $date

            {"success": "true"}
            EOT
        );
    }

    public function test_logSeverityErrorBadRequestResponse()
    {
        $request = Request::create('https://fittinq.com/errorOnBadRequest', Request::METHOD_GET);
        $response = new Response('{"success": "false"}', Response::HTTP_BAD_REQUEST, []);
        $date = $response->getDate()->format('D, d M Y H:i:s'). ' GMT';

        $this->eventSubscriber = $this->configuration->configure($request, $response, new LoggingAwareController());
        $this->onKernelController();
        $this->onKernelResponse();

        $this->loggerMock->expectToHaveLoggedLast(
            LogLevel::ERROR,
            <<<EOT
            GET https://fittinq.com/errorOnBadRequest

            HTTP 400

            cache-control: no-cache, private
            date: $date

            {"success": "false"}
            EOT
        );
    }

    public function test_logSeverityCriticalOnInternalServerErrorResponse()
    {
        $request = Request::create('https://fittinq.com/onInternalServerError', Request::METHOD_GET);
        $response = new Response('{"success": "false"}', Response::HTTP_INTERNAL_SERVER_ERROR, []);
        $date = $response->getDate()->format('D, d M Y H:i:s'). ' GMT';

        $this->eventSubscriber = $this->configuration->configure($request, $response, new LoggingAwareController());
        $this->onKernelController();
        $this->onKernelResponse();

        $this->loggerMock->expectToHaveLoggedLast(
            LogLevel::CRITICAL,
            <<<EOT
            GET https://fittinq.com/onInternalServerError

            HTTP 500

            cache-control: no-cache, private
            date: $date

            {"success": "false"}
            EOT
        );
    }

    public function test_catchExceptionWhenLogging()
    {
        $request = Request::create('https://fittinq.com/loggingFailsWithException_catchException', Request::METHOD_POST);
        $response = new Response(
            '{"success": "true"}',
            Response::HTTP_OK,
            [
                "content-type" => 'application/json',
                "x-auth-token" => 'token_0123456789',
                "authorization" => '[type] [credentials]'
            ]
        );
        $this->loggerMock->setUpWithException($this->createMock(Throwable::class));

        $this->eventSubscriber = $this->configuration->configure($request, $response, new LoggingAwareController());
        $this->onKernelController();
        $this->onKernelResponse();

        $this->assertTrue(true);
    }

    private function onKernelController(): void
    {
        $beforeControllerEvent = $this->configuration->getBeforeControllerEvent();
        $this->eventSubscriber->onKernelController($beforeControllerEvent);
    }

    private function onKernelResponse(): void
    {
        $afterControllerEvent = $this->configuration->getAfterControllerEvent();
        $this->eventSubscriber->onKernelResponse($afterControllerEvent);
    }
}
