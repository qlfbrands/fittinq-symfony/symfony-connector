<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

interface IndexAwareInterface
{
    public function index();
}
