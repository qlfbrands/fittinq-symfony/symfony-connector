<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

use Fittinq\Symfony\Connector\Logging\LoggingAwareInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LoggingAwareController extends AbstractController implements LoggingAwareInterface, IndexAwareInterface
{
    public function index()
    {
    }
}