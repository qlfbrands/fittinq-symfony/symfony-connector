<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\HttpClient;

use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Test\Fittinq\Symfony\Connector\Logging\LoggerMock;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;
use Test\Fittinq\Symfony\Mock\HttpClient\ResponseMock;
use Throwable;

class ContextTest extends TestCase
{
    private HttpClientInterface $httpClient;
    private LoggerMock $logger;
    private HttpClientMock $decoratedHttpClient;
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->logger = $this->configuration->getLogger();
        $this->decoratedHttpClient = $this->configuration->getDecoratedHttpClient();
    }

    /**
     * @throws Throwable
     */
    public function test_httpRequestsLogContextThatContainsAUniqueRequestId()
    {
        $this->httpClient = $this->configuration->configure();
        $this->decoratedHttpClient->setUpResponse(new ResponseMock());

        $this->httpClient->request('GET', 'https://www.fittinq.com/test_httpRequestsLogContextThatContainsAUniqueRequestId',);

        $this->logger->expectToHaveLoggedContextAtIndex(["requestId" => '/[0-f]{13}/'], 0);
        $this->logger->expectToHaveLoggedContextAtIndex(["requestId" => "/[0-f]{13}/"], 1);

    }

    /**
     * @throws Throwable
     */
    public function test_loggedRequestIdsShouldBeEqualForRequestAndResponse()
    {
        $this->httpClient = $this->configuration->configure();
        $this->decoratedHttpClient->setUpResponse(new ResponseMock());
        $this->httpClient->request('GET', 'https://www.fittinq.com/test_loggedRequestIdsShouldBeEqualForRequestAndResponse',);

        $this->logger->expectContextRequestIdsToBeEqual(0, 1);
    }

    /**
     * @throws Throwable
     */
    public function test_loggedRequestIdsShouldNotBeEqualForSubsequentRequests()
    {
        $this->httpClient = $this->configuration->configure();
        $this->decoratedHttpClient->setUpResponse(new ResponseMock());
        $this->httpClient->request('GET', 'https://www.fittinq.com/test_loggedRequestIdsShouldNotBeEqualForSubsequentRequests1');
        $this->httpClient->request('GET', 'https://www.fittinq.com/test_loggedRequestIdsShouldNotBeEqualForSubsequentRequests2');
        $this->httpClient->request('GET', 'https://www.fittinq.com/test_loggedRequestIdsShouldNotBeEqualForSubsequentRequests3');

        $this->logger->expectContextRequestIdsToBeEqual(0, 1);
        $this->logger->expectContextRequestIdsToBeEqual(2, 3);
        $this->logger->expectContextRequestIdsToBeEqual(4, 5);

        $this->logger->expectContextRequestIdsNotToBeEqual(0, 2);
        $this->logger->expectContextRequestIdsNotToBeEqual(0, 4);
        $this->logger->expectContextRequestIdsNotToBeEqual(2, 4);
    }
}