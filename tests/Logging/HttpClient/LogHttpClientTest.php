<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\HttpClient;

use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Test\Fittinq\Symfony\Mock\HttpClient\ResponseMock;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;
use Throwable;

class LogHttpClientTest extends TestCase
{
    private HttpClientInterface $httpClient;
    private LoggerMock $logger;
    private HttpClientMock $decoratedHttpClient;
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->logger = $this->configuration->getLogger();
        $this->decoratedHttpClient = $this->configuration->getDecoratedHttpClient();
    }

    /**
     * @throws Throwable
     */
    public function test_makePostRequest_logRequest()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(HttpResponse::HTTP_OK, [], '{"success":true}');
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request(
            'POST',
            'https://www.fittinq.com/makePostRequest?bicycle=noTrain',
            [
                'headers' => [
                    'content-type' => 'plain/text',
                    'x-auth-token' => 'token_abcdef',
                    'authorization' => '[type] [credentials]'
                ],
                'json' => [
                    "number" => 1234
                ],
                'query' => [
                    "car" => "noTrainEither"
                ]
            ]
        );

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            POST https://www.fittinq.com/makePostRequest?bicycle=noTrain&car=noTrainEither

            content-type: plain/text
            x-auth-token: token_abcdef
            authorization: [type] [credentials]

            {"number":1234}
            EOT,
            0
        );
    }

    /**
     * @throws Throwable
     */
    public function test_makeGetRequestWithoutBody_logRequest()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(HttpResponse::HTTP_OK, [], '{"success":true}');
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request(
            'GET',
            'https://www.fittinq.com/makeGetRequestWithoutBody?bicycle=noTrain',
            [
                'headers' => [
                    'content-type' => 'plain/text',
                    'x-auth-token' => 'token_012345',
                ],
                'query' => [
                    "car" => "noTrainEither"
                ]
            ]
        );

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            GET https://www.fittinq.com/makeGetRequestWithoutBody?bicycle=noTrain&car=noTrainEither

            content-type: plain/text
            x-auth-token: token_012345
            EOT,
            0
        );
    }

    /**
     * @throws Throwable
     */
    public function test_makeRequest_logResponse()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(
            HttpResponse::HTTP_OK,
            [
                'content-type' => 'plain/text',
                'x-auth-token' => 'token_012345',
            ],
            '{"success":true}'
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request('GET', 'https://www.fittinq.com/makeGetRequestWithoutBody?bicycle=noTrain',
            [
                'query' => ["car" => "noTrainEither"]
            ]
        );

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            GET https://www.fittinq.com/makeGetRequestWithoutBody?bicycle=noTrain&car=noTrainEither

            HTTP 200

            content-type: plain/text
            x-auth-token: token_012345
            
            {"success":true}
            EOT,
            1
        );
    }

    /**
     * @throws Throwable
     */
    public function test_errorOnBadRequest_logResponseWithSeverityError()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(
            HttpResponse::HTTP_BAD_REQUEST,
            [],
            '{"success":false}'
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request('GET', 'https://www.fittinq.com/errorOnBadRequest', []);

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::ERROR,
            <<<EOT
            GET https://www.fittinq.com/errorOnBadRequest

            HTTP 400

            {"success":false}
            EOT,
            1
        );
    }

    /**
     * @throws Throwable
     */
    public function test_onInternalServerError_logResponseWithSeverityCritical()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(
            HttpResponse::HTTP_INTERNAL_SERVER_ERROR,
            [],
            '{"success":false}'
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request('GET', 'https://www.fittinq.com/onInternalServerError', []);

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::CRITICAL,
            <<<EOT
            GET https://www.fittinq.com/onInternalServerError

            HTTP 500

            {"success":false}
            EOT,
            1
        );
    }

    /**
     * @throws Throwable
     */
    public function test_onUnavailableService_logSeverityCritical()
    {
        try {
            $this->httpClient = $this->configuration->configure();
            $response = new ResponseMock(
                0,
                [],
                ''
            );
            $this->decoratedHttpClient->setUpResponse($response);
            $this->httpClient->request('GET', 'https://www.fittinq.com/onInternalServerError');
        } catch (TransportException $e) {
            $message = $e->getMessage();
            $this->logger->expectToHaveLoggedAtIndex(
                LogLevel::CRITICAL,
            <<<EOT
            GET https://www.fittinq.com/onInternalServerError

            Service is unavailable: $message
            EOT,
                1
            );
        }


    }

    /**
     * @throws Throwable
     */
    public function test_onUnavailableService_rethrowTransportException()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(
            0,
            [],
            ''
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->expectException(TransportException::class);
        $this->httpClient->request('GET', 'https://www.fittinq.com/onInternalServerError', []);
    }

    /**
     * @throws Throwable
     */
    public function test_loggingFailsWithException_catchException()
    {
        $this->httpClient = $this->configuration->configure();
        $response = new ResponseMock(
            HttpResponse::HTTP_OK,
            [
                'content-type' => 'plain/text',
                'x-auth-token' => 'token_012345',
            ],
            '{"success":true}'
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->logger->setUpWithException($this->createMock(Throwable::class));

        $this->assertEquals($response, $this->httpClient->request(Request::METHOD_GET, 'https://www.fittinq.com/loggingFailsWithException', []));
        $this->decoratedHttpClient->expectRequestToHaveBeenMadeWithMethod(Request::METHOD_GET);

        $this->decoratedHttpClient->expectRequestToHaveBeenMadeTo('https://www.fittinq.com/loggingFailsWithException');
    }

    /**
     * @throws Throwable
     */
    public function test_censorSensitiveHeadersForRequest()
    {
        $this->httpClient = $this->configuration->configure(
            [],
            ['x-auth-token', 'authorization']
        );
        $response = new ResponseMock(HttpResponse::HTTP_OK, [], '{"success":true}');
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request(
            'POST',
            'https://www.fittinq.com/test_censorSensitiveHeadersForRequest',
            [
                'headers' => [
                    'content-type' => 'plain/text',
                    'x-auth-token' => 'token_abcdef',
                    'authorization' => '[type] [credentials]'
                ],
                'json' => [
                    "number" => 1234
                ]
            ]
        );

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            POST https://www.fittinq.com/test_censorSensitiveHeadersForRequest

            content-type: plain/text
            x-auth-token: *******
            authorization: *******

            {"number":1234}
            EOT,
            0
        );
    }


    /**
     * @throws Throwable
     */
    public function test_censorSensitiveHeadersForResponse()
    {
        $this->httpClient = $this->configuration->configure(
            [],
            ['x-auth-token', 'authorization']
        );
        $response = new ResponseMock(
            HttpResponse::HTTP_OK,
            [
                'content-type' => 'plain/text',
                'x-auth-token' => 'token_012345',
                'authorization' => '[type] [credentials]',
            ],
            '{"success":true}'
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request('GET', 'https://www.fittinq.com/test_censorSensitiveHeadersForResponse');

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            GET https://www.fittinq.com/test_censorSensitiveHeadersForResponse

            HTTP 200

            content-type: plain/text
            x-auth-token: *******
            authorization: *******
            
            {"success":true}
            EOT,
            1
        );
    }

    /**
     * @throws Throwable
     */
    public function test_censorSensitiveBodyAndHeadersForRequestWhenTheURLIsSensitive()
    {
        $this->httpClient = $this->configuration->configure(
            ['https://www.fittinq.com/test_censorSensitiveBodyForRequestWhenTheURLIsSensitive']
        );
        $response = new ResponseMock();
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request(
            'GET',
            'https://www.fittinq.com/test_censorSensitiveBodyForRequestWhenTheURLIsSensitive',
            [
                'headers' => [
                    'content-type' => 'plain/text',
                    'x-auth-token' => 'token_012345',
                ],
                'json' => [
                    "censor" => 1234
                ],
            ],
        );

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            GET https://www.fittinq.com/test_censorSensitiveBodyForRequestWhenTheURLIsSensitive

            content-type: *******
            x-auth-token: *******
            
            *******
            EOT,
            0
        );
    }

    /**
     * @throws Throwable
     */
    public function test_censorSensitiveBodyAndHeadersForResponseWhenTheURLIsSensitive()
    {
        $this->httpClient = $this->configuration->configure(
            ['https://www.fittinq.com/test_censorSensitiveBodyForResponseWhenTheURLIsSensitive'],
        );
        $response = new ResponseMock(
            HttpResponse::HTTP_OK,
            [
                'content-type' => 'plain/text',
                'x-auth-token' => 'token_012345',
                'authorization' => '[type] [credentials]',
            ],
            '{"success":true}'
        );
        $this->decoratedHttpClient->setUpResponse($response);
        $this->httpClient->request('GET', 'https://www.fittinq.com/test_censorSensitiveBodyForResponseWhenTheURLIsSensitive');

        $this->logger->expectToHaveLoggedAtIndex(
            LogLevel::INFO,
            <<<EOT
            GET https://www.fittinq.com/test_censorSensitiveBodyForResponseWhenTheURLIsSensitive

            HTTP 200

            content-type: *******
            x-auth-token: *******
            authorization: *******
            
            *******
            EOT,
            1
        );
    }
}
