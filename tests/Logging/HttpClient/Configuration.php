<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\HttpClient;

use Fittinq\Symfony\Connector\HttpClient\LogHttpClient;
use Fittinq\Symfony\Connector\Logging\HttpClientLogger;
use Fittinq\Symfony\Connector\Logging\MessageFormatter;
use Test\Fittinq\Symfony\Connector\Logging\LoggerMock;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;

class Configuration
{
    private LoggerMock $loggerMock;
    private HttpClientMock $decoratedHttpClient;

    public function __construct()
    {
        $this->loggerMock = new LoggerMock();
        $this->decoratedHttpClient = new HttpClientMock();
    }

    public function configure(array $sensitiveUrls = [], array $sensitiveHeaders = []): LogHttpClient
    {
        $messageFormatter = new MessageFormatter($sensitiveUrls, $sensitiveHeaders);
        $httpClientLogger = new HttpClientLogger($this->loggerMock, $messageFormatter);
        return new LogHttpClient($this->decoratedHttpClient, $httpClientLogger);
    }

    public function getLogger(): LoggerMock
    {
        return $this->loggerMock;
    }

    public function getDecoratedHttpClient(): HttpClientMock
    {
        return $this->decoratedHttpClient;
    }
}
