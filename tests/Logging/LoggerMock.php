<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging;

use PHPUnit\Framework\Assert;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock as SymfonyMockLoggerMock;

class LoggerMock extends SymfonyMockLoggerMock
{

    public function expectToHaveLoggedContextAtIndex(array $context, int $i): void
    {
        foreach ($context as $key => $value) {
            Assert::assertMatchesRegularExpression($value, $this->contexts[$i][$key]);
        }
    }

    public function expectContextRequestIdsToBeEqual(int $requestIndex, int $responseIndex): void
    {
        Assert::assertEquals($this->contexts[$requestIndex]['requestId'], $this->contexts[$responseIndex]['requestId']);
    }

    public function expectContextRequestIdsNotToBeEqual(int $requestIndex, int $responseIndex): void
    {
        Assert::assertNotEquals($this->contexts[$requestIndex]['requestId'], $this->contexts[$responseIndex]['requestId']);
    }
}